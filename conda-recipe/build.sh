#!/bin/bash
mkdir build
cd build

cmake ../ -DCMAKE_INSTALL_PREFIX=$PREFIX\
	-DCMAKE_PREFIX_PATH=$PREFIX\
	-DNOVATEL_BUILD_EXAMPLES=OFF\
	-DNOVATEL_BUILD_TESTS=OFF\
	-DBUILD_WITH_ROS=OFF

make -j4
make install

cp $RECIPE_DIR/prefix/* $PREFIX -r
