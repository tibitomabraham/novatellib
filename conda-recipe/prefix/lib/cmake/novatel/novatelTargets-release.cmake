#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "Novatel::novatelAPI" for configuration "Release"
set_property(TARGET Novatel::novatelAPI APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(Novatel::novatelAPI PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libnovatel.a"
  IMPORTED_SONAME_RELEASE "libnovatel.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS Novatel::novatelAPI )
list(APPEND _IMPORT_CHECK_FILES_FOR_Novatel::novatelAPI "${_IMPORT_PREFIX}/lib/libnovatel.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
